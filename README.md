# BlackmouthGamesTest

## Prueba de gameplay - Servidor dedicado

En este documento se reflejan las secciones que se piden desarrollar, la solución escogida en cada una de ellas y una breve explicación de su implementación así como los detalles relevantes correspondientes.


## Sección 1

**- Ver en su pantalla la animación de disparo del otro jugador**

Para replicar la animación del personaje y que los demás jugadores la vean, hacemos uso de los RPCs, en este caso necesitamos dos:
    
    - Replicación del cliente al servidor: UFUNCTION(Server, Reliable)
    - Replicación del servidor a los clientes: UFUNCTION(NetMulticast, Reliable)

De esta manera el cliente pide al servidor ejecutar la animación y este, la replica a todos los clientes conectados. Como resultado final todos los jugadores ven la animación del disparo, incluido el que controla el jugador. Se implementa en la clase **BlackmouthGamesCharacter** en _OnFirePlayAnimation_.

**- Ver el proyectil resultante del disparo del otro jugador**

Para replicar el proyectil, configuramos que el que se encargue de hacer el _spawn_ del proyectil sea el servidor para, nuevamente usar los _RPCs_ pero en este caso, si eres el cliente pides al servidor que haga el _spawn_ del proyectil sin hacer _multicast_, ya que estaríamos entonces spawneando un proyectil en cada cliente y solo queremos que haya un proyectil que todos vean.


**- Añadir algún elemento visual como “cuerpo” del jugador, para poder discernir mejor su
posición**

Para este apartado hacemos visible el static mesh del personaje para todos los jugadores, los brazos sujetando el arma. En este caso debemos desmarcar la opción _Only Owner See_ dentro de _Rendering_ del _static mesh_ del personaje. Todos los jugadores se ven entre ellos. Se implementa en el blueprint **FirstPersonCharacter** en _Content/FirstPersonCPP/Blueprints_.




## Sección 2

**- Crea un componente de Vida con las siguientes características**

    - Dañar al jugador una cantidad específica
    - Curar al jugador una cantidad específica
    - Una función que devuelva la vida normalizada de 0 a 1
    - Una función que devuelva la vida restante, sin normalizar

Este componente de vida tendrá una variable"_Health_ con la propiedad _UPROPERTY(Replicated)_, haciendo _override_ de la función _GetLifetimeReplicatedProps_ donde, incorporaremos la variable para que su valor se replique. De igual manera que con la animación, para actualizar el valor de la salud haremos uso de los _RPCs_:

    - Replicación del cliente al servidor: UFUNCTION(Server, Reliable)
    - Replicación del servidor a los clientes: UFUNCTION(NetMulticast, Reliable)

Con la salud configurada, tendremos una función _TakeDamage_ y otra _CureHealth_ que harán llamada a estos _RPCs_ para cambiar el valor de la salud bajo ciertas condiciones, controlando los valores mínimos y máximos que se pongan.

Las funciones que devuelven la vida normalizada y la vida resultante van bajo la propiedad _UFUNCTION(BlueprintCallable, Category = "Health")_ para que se puedan llamar desde blueprints, como el widget que muestra la salud. Se implementa en la clase **HealthComponent**.

Para curar al jugador, se ha implementado una acción de curar pulsando la tecla "**C**".



## Sección 3

**- Implementar que el disparo dañe al jugador enemigo**

Recordemos que el server es quien se encarga de hacer el daño, el valor de la salud se replica, no necesitamos una _RPC_ en este caso para replicar el daño, el servidor hace el daño, cambia el valor de la salud, los clientes ya reciben esa información. Si el cliente hiciera el daño, si necesitamos usar los _RPCs_ para que le llegara la información al servidor.

Las funciones de daño se han implementado en la sección anterior, comprobamos que el _overlap_ del proyectil corresponde a un jugador enemigo y llamamos a _TakeDamage_. Se implementa en la clase **BlackmouthGamesProjectile** en _OnHit_.



## Sección 4

**- Crear y añadir un widget de barra de vida**

La creación y manejo del widget lo haremos a través de blueprints. La función de obtener la vida en porcentaje la tenemos, la usamos en una _progress bar_.

Se crean eventos para mostrar en rojo el daño recibido y en verde la cura recibida. Estos eventos se llaman cuando el personaje recibe daño o recibe la cura, a través de funciones con la propiedad _UFUNCTION(BlueprintImplementableEvent, Category = "Health")_. Se implementa en el blueprint **HealthBar_BP** en _Content/UI_


## Sección 5

**- Implementar un componente que contenga una habilidad ofensiva**

Esta habilidad ofensiva tiene las siguientes características:

    - Esfera centrada en el dueño que crece hasta un máximo mientras él mantenga pulsada una tecla
    - Tras soltar la tecla, hace daño a las entidades enemigas que se encuentren dentro
    - Una vez usada, esa habilidad entra en _cooldown_ durante X segundos

La idea principal era usar un _static mesh_ para que se viera una esfera de un color, sin embargo el tema de las colisiones me ha dado problemas así como los eventos de _overlap_, finalmente he optado por usar una _sphere collision_ y mostrarla cuando se pulsa la tecla.

Aqui hacemos uso de los eventos de **BeginOverlap** y **EndOverlap**, añadiendo los jugadores que esten dentro de la esfera, y eliminándolos en caso de que salgan. al soltar el botón se hará daño a quien se encuentre dentro. Este daño sí lo tenemos que replicar, como en el caso de la animación, usando los siguientes _RPCs_:

    - Replicación del cliente al servidor: UFUNCTION(Server, Reliable)
    - Replicación del servidor a los clientes: UFUNCTION(NetMulticast, Reliable)

En este caso es el cliente quien realiza el daño, el cliente debe mandar esta información al servidor para que la ejecute y la replique a todos los jugadores. El _cooldown_ lo implementamos haciendo uso de un **FTimer**, junto a una función que devuelve el estado actual del _cooldown_.

El crecimiento de la esfera lo controlamos desde el event **Tick** del componente, replicando el tamaño de la esfera nuevamente con el uso de los _RPCs_ anteriores, para que todos los jugadores vean el tamaño de la esfera según crece. Creamos una función que devuelve el estado de crecimiento de la esfera (habilidad). Se implementa en la clase **SphereAttackComponent**.

La habilidad ofensiva se ha implementado para usarse pulsando la tecla "**R**".


## Sección 6

**- Crear un widget que muestra una barra de progreso de esa habilidad**

EL widget nuevamente está formado por una _progress bar_. Haciendo uso de la función de la sección anterior, que devuelve el progreso de la habilidad, lo mostramos en pantalla, junto al estado del _cooldown_. Se implementa en el blueprint **HealthBar_BP** en _Content/UI_.


## Anexo

Se ha desarrollado el código de tal manera que leyendo el nombre de la función y de las variables que se usan, el lector sea capaz de identificar qué funcionalidad se está implementado, evitando así llenar el código de comentarios.

Se evitan los **magic numbers** sacando estos valores a las cabeceras con nombres descriptivos, pudiendo así simplemente cambiar este valor fijo en cabecera para ajustar los parámetros o simplemente testear mas posibilidades.

Se pedía una implementación que fuera valida para usar en un **servidor dedicado**, por lo que antes de iniciar el desarrollo he creado un servidor dedicado siguiendo los siguientes pasos:


    - Bajar y compilar la build de Unreal Engine del github, versión 4.25
    - Configurar el Server Build Target
    - Crear un mapa dentro del editor que llame a **localhost**
    - Configuramos los mapas por defecto, el del servidor será el del **FirstPerson**, los clientes al nuevo mapa


De esta manera lo tenemos preparado, para genera un .exe del servidor y un .exe del juego para conectarte. Se ha seguido la siguiente guía: (https://docs.unrealengine.com/en-US/InteractiveExperiences/Networking/HowTo/DedicatedServers/index.html).

Hasta ahora en mi proyecto personal he estado usando a uno de los jugadores actuando de servidor y el resto, de clientes. Esta prueba me ha permitido ampliar conocimientos y aprender más formas, usando un servidor dedicado, el cual a futuro, integraré en mis proyectos para poder escalarlo a más jugadores.


## Funcionamiento

- Descomprimir el archivo **Exe.zip**
- Ejecución de **BlackmouthGamesServer.exe -FirstPersonExampleMap.exe** dentro de _Exe/WindowsServer_
- Ejecución de **BlackmouthGames.exe** dentro de _Exe/WindowsNoEditor_
