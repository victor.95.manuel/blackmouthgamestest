// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BlackmouthGamesHUD.generated.h"

UCLASS()
class ABlackmouthGamesHUD : public AHUD
{
	GENERATED_BODY()

public:
	ABlackmouthGamesHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
};
