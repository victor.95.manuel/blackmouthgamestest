// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlackmouthGamesCharacter.h"
#include "BlackmouthGamesProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "SphereAttackComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

#define STANDARD_ANIM_DURATION 1.0f

//////////////////////////////////////////////////////////////////////////
// ABlackmouthGamesCharacter

ABlackmouthGamesCharacter::ABlackmouthGamesCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true); // only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true); // only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f)); // Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void ABlackmouthGamesCharacter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!HasAuthority()) return;

	HandlePlayerRotationServer(GetControlRotation());
}

void ABlackmouthGamesCharacter::HandlePlayerRotationServer_Implementation(const FRotator RotationToApply)
{
	if (!IsValid(FirstPersonCameraComponent)) return;

	FirstPersonCameraComponent->SetWorldRotation(RotationToApply);
}

void ABlackmouthGamesCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P,
	                          FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true),
	                          TEXT("GripPoint")
	);

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void ABlackmouthGamesCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABlackmouthGamesCharacter::OnFire);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ABlackmouthGamesCharacter::OnResetVR);

	PlayerInputComponent->BindAction(
		"SphereAttack",
		IE_Pressed,
		this,
		&ABlackmouthGamesCharacter::SphereAttackBegin
	);

	PlayerInputComponent->BindAction(
        "SphereAttack",
        IE_Released,
        this,
        &ABlackmouthGamesCharacter::SphereAttackEnd
    );

	PlayerInputComponent->BindAction(
        "Cure",
        IE_Pressed,
        this,
        &ABlackmouthGamesCharacter::OnCurePlayer
    );

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ABlackmouthGamesCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABlackmouthGamesCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABlackmouthGamesCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABlackmouthGamesCharacter::LookUpAtRate);
}

void ABlackmouthGamesCharacter::SpawnBulletUsingMotionControllers()
{
	if (!bUsingMotionControllers ||
		!IsValid(GetWorld()) ||
		!IsValid(ProjectileClass))
		return;

	const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
	const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();

	GetWorld()->SpawnActor<ABlackmouthGamesProjectile>(
		ProjectileClass,
		SpawnLocation,
		SpawnRotation
	);
}

void ABlackmouthGamesCharacter::SpawnBulletWithoutMotionControllers()
{
	if (bUsingMotionControllers ||
		!IsValid(GetWorld()) ||
		!IsValid(ProjectileClass))
		return;

	const FRotator SpawnRotation = GetControlRotation();

	// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
	const FVector SpawnLocation = (FP_MuzzleLocation != nullptr
		                               ? FP_MuzzleLocation->GetComponentLocation()
		                               : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

	//Set Spawn Collision Handling Override
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride =
		ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	GetWorld()->SpawnActor<ABlackmouthGamesProjectile>(
		ProjectileClass,
		SpawnLocation,
		SpawnRotation,
		ActorSpawnParams
	);
}

void ABlackmouthGamesCharacter::HandleOnFireSpawnBulletClient_Implementation()
{
	SpawnBulletUsingMotionControllers();
	SpawnBulletWithoutMotionControllers();
}

void ABlackmouthGamesCharacter::OnFireSpawnBullet()
{
	if (HasAuthority())
	{
		SpawnBulletUsingMotionControllers();
		SpawnBulletWithoutMotionControllers();
	}
	else
		HandleOnFireSpawnBulletClient();
}

void ABlackmouthGamesCharacter::OnFirePlaySound() const
{
	if (!IsValid(FireSound)) return;

	UGameplayStatics::PlaySoundAtLocation(
		this,
		FireSound,
		GetActorLocation()
	);
}

void ABlackmouthGamesCharacter::OnFirePlayAnimation()
{
	if (HasAuthority())
		HandleOnFirePlayAnimationServer();
	else
		HandleOnFirePlayAnimationClient();
}

void ABlackmouthGamesCharacter::HandleOnFirePlayAnimationServer_Implementation()
{
	if (!IsValid(FireAnimation) || !IsValid(Mesh1P)) return;

	UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();

	if (!IsValid(AnimInstance)) return;

	AnimInstance->Montage_Play(
		FireAnimation,
		STANDARD_ANIM_DURATION
	);
}

void ABlackmouthGamesCharacter::HandleOnFirePlayAnimationClient_Implementation()
{
	HandleOnFirePlayAnimationServer();
}

void ABlackmouthGamesCharacter::HandleCurePlayerServer_Implementation()
{
	CurePlayer();
}

void ABlackmouthGamesCharacter::HandleCurePlayerClient_Implementation()
{
	HandleCurePlayerServer();
}

void ABlackmouthGamesCharacter::SphereAttackBegin()
{
	USphereAttackComponent* SphereAttackComponent = GetSphereAttackComponent();

	if (!IsValid(SphereAttackComponent)) return;

	SphereAttackComponent->SphereAttackBegin();
}

void ABlackmouthGamesCharacter::SphereAttackEnd()
{
	USphereAttackComponent* SphereAttackComponent = GetSphereAttackComponent();

	if (!IsValid(SphereAttackComponent)) return;

	SphereAttackComponent->SphereAttackEnd();
}

void ABlackmouthGamesCharacter::OnCurePlayer()
{
	if (HasAuthority())
		HandleCurePlayerServer();
	else
		HandleCurePlayerClient();
}

void ABlackmouthGamesCharacter::CurePlayer()
{
	if (!IsValid(GetHealthComponent()) || !GetHealthComponent()->GetIsAlive()) return;

	GetHealthComponent()->CureHealth();
}

void ABlackmouthGamesCharacter::OnFire()
{
	OnFireSpawnBullet();
	OnFirePlaySound();
	OnFirePlayAnimation();
}

void ABlackmouthGamesCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ABlackmouthGamesCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ABlackmouthGamesCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ABlackmouthGamesCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void ABlackmouthGamesCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ABlackmouthGamesCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ABlackmouthGamesCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABlackmouthGamesCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool ABlackmouthGamesCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ABlackmouthGamesCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ABlackmouthGamesCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ABlackmouthGamesCharacter::TouchUpdate);
		return true;
	}

	return false;
}

UHealthComponent* ABlackmouthGamesCharacter::GetHealthComponent() const
{
	TArray<UHealthComponent*> HealthComponents;
	GetComponents(HealthComponents);

	for (UHealthComponent* Component : HealthComponents)
	{
		if (!IsValid(Component)) continue;

		return Component;
	}

	return nullptr;
}

USphereAttackComponent* ABlackmouthGamesCharacter::GetSphereAttackComponent() const
{
	TArray<USphereAttackComponent*> SphereAttackComponents;
	GetComponents(SphereAttackComponents);

	for (USphereAttackComponent* Component : SphereAttackComponents)
	{
		if (!IsValid(Component)) continue;

		return Component;
	}

	return nullptr;
}
