// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlackmouthGames.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, BlackmouthGames, "BlackmouthGames");
