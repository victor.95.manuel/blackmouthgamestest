// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"


#include "BlackmouthGamesCharacter.h"
#include "BlackmouthGamesGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = InitialHealth;
}

void UHealthComponent::TickComponent(const float DeltaTime,
                                     const ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UHealthComponent::GetCurrentHealthPercentage() const
{
	return
		static_cast<float>(Health) / static_cast<float>(InitialHealth);
}

void UHealthComponent::HandleRespawnPlayerServer_Implementation()
{
	if (!IsValid(GetOwner()) ||
		!IsValid(GetWorld()) ||
		!IsValid(GetOwner()->GetInstigatorController()))
		return;

	AGameModeBase* CurrentGameMode = UGameplayStatics::GetGameMode(GetOwner()->GetWorld());

	if (!CurrentGameMode) return;

	ABlackmouthGamesGameMode* CustomGameMode = Cast<ABlackmouthGamesGameMode>(CurrentGameMode);

	if (!IsValid(CustomGameMode)) return;

	CustomGameMode->TimerRespawnPlayer(GetOwner()->GetInstigatorController());

	GetOwner()->Destroy();
}

void UHealthComponent::SetupRespawnPlayer() const
{
	if (!IsValid(GetOwner()) ||
		!IsValid(GetWorld()) ||
		!IsValid(GetOwner()->GetInstigatorController()) ||
		!GetOwner()->HasAuthority())
		return;

	AGameModeBase* CurrentGameMode = UGameplayStatics::GetGameMode(GetOwner()->GetWorld());

	if (!CurrentGameMode) return;

	ABlackmouthGamesGameMode* CustomGameMode = Cast<ABlackmouthGamesGameMode>(CurrentGameMode);

	if (!IsValid(CustomGameMode)) return;

	CustomGameMode->TimerRespawnPlayer(GetOwner()->GetInstigatorController());

	GetOwner()->Destroy();
}

void UHealthComponent::CharacterDeath() const
{
	if (Health > 0) return;

	SetupRespawnPlayer();
}

void UHealthComponent::TakeDamage(const int32 DamageToApply)
{
	Health = FMath::Max(
		Health - DamageToApply,
		MinHealth
	);

	ShowDamage(DamageToApply);
	CharacterDeath();
}

void UHealthComponent::ShowDamage(const int32 DamageToShow) const
{
	if (!IsValid(GetOwner())) return;

	ABlackmouthGamesCharacter* CustomCharacter = Cast<ABlackmouthGamesCharacter>(GetOwner());

	if (!IsValid(CustomCharacter) || !CustomCharacter->IsLocallyControlled()) return;

	CustomCharacter->ShowDamage(DamageToShow);
}

void UHealthComponent::CureHealth()
{
	Health = FMath::Min(
		Health + HealthToCure,
		InitialHealth
	);

	ShowCure(HealthToCure);
}

void UHealthComponent::ShowCure(const int32 CureToShow) const
{
	if (!IsValid(GetOwner())) return;

	ABlackmouthGamesCharacter* CustomCharacter = Cast<ABlackmouthGamesCharacter>(GetOwner());

	if (!IsValid(CustomCharacter) || !CustomCharacter->IsLocallyControlled()) return;

	CustomCharacter->ShowCure(CureToShow);
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
}
