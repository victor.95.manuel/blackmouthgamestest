// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlackmouthGamesGameMode.h"
#include "BlackmouthGamesHUD.h"
#include "BlackmouthGamesCharacter.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

#define TIME_TO_RESPAWN 10.0f

ABlackmouthGamesGameMode::ABlackmouthGamesGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
		TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter")
	);

	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABlackmouthGamesHUD::StaticClass();
}

TArray<ATargetPoint*> ABlackmouthGamesGameMode::FindTargetPointsToRespawn() const
{
	TArray<AActor*> ActorsFound;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), ActorsFound);

	TArray<ATargetPoint*> TargetPointsFound;

	for (AActor* Actor : ActorsFound)
	{
		if (!IsValid(Actor)) continue;

		ATargetPoint* TargetPoint = Cast<ATargetPoint>(Actor);

		if (!IsValid(TargetPoint)) continue;

		TargetPointsFound.Add(TargetPoint);
	}

	return TargetPointsFound;
}

TArray<APlayerStart*> ABlackmouthGamesGameMode::FindPlayerStart() const
{
	TArray<AActor*> ActorsFound;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), ActorsFound);

	TArray<APlayerStart*> PlayerStartFound;

	for (AActor* Actor : ActorsFound)
	{
		if (!IsValid(Actor)) continue;

		APlayerStart* PlayerStarPoint = Cast<APlayerStart>(Actor);

		if (!IsValid(PlayerStarPoint)) continue;

		PlayerStartFound.Add(PlayerStarPoint);
	}

	return PlayerStartFound;
}

FTransform ABlackmouthGamesGameMode::GetPlayerStart() const
{
	TArray<ATargetPoint*> TargetPointsToRespawn = FindTargetPointsToRespawn();

	if (!TargetPointsToRespawn.IsValidIndex(0)) return FTransform();

	float MinXValue = TargetPointsToRespawn[0]->GetActorLocation().X;
	float MaxXValue = TargetPointsToRespawn[0]->GetActorLocation().X;
	float MinYValue = TargetPointsToRespawn[0]->GetActorLocation().Y;
	float MaxYValue = TargetPointsToRespawn[0]->GetActorLocation().Y;

	for (ATargetPoint* Point : TargetPointsToRespawn)
	{
		if (!IsValid(Point)) continue;

		const float XValue = Point->GetActorLocation().X;
		const float YValue = Point->GetActorLocation().Y;

		if (XValue < MinXValue)
			MinXValue = XValue;

		if (XValue > MaxXValue)
			MaxXValue = XValue;

		if (YValue < MinYValue)
			MinYValue = YValue;

		if (YValue > MaxYValue)
			MaxYValue = YValue;
	}

	const float XValue = FMath::RandRange(MinXValue, MaxXValue);
	const float YValue = FMath::RandRange(MinYValue, MaxYValue);
	const float ZValue = TargetPointsToRespawn[0]->GetActorLocation().Z;

	const FVector LocationToTransform = FVector(
		XValue,
		YValue,
		ZValue
	);

	const FRotator RotationToTransform = FRotator(
		0.0f,
		0.0f,
		0.0f
	);

	const FVector ScaleToTransform = FVector(
		1.0f,
		1.0f,
		1.0f
	);

	return
		FTransform(
			RotationToTransform,
			LocationToTransform,
			ScaleToTransform
		);
}

void ABlackmouthGamesGameMode::SpawnPlayer(APlayerController* NewPlayer)
{
	if (!IsValid(ActorToSpawn) ||
		// !IsValid(GetWorld()) ||
		!IsValid(NewPlayer))
		return;

	APlayerStart* PlayerStartPoint = FindPlayerStart()[IndexToSpawn];

	if (!FindPlayerStart().IsValidIndex(IndexToSpawn)) return;

	ACharacter* ActorSpawned = GetWorld()->SpawnActor<ACharacter>(
		ActorToSpawn,
		PlayerStartPoint->GetActorLocation(),
		PlayerStartPoint->GetActorRotation()
	);

	if (!IsValid(ActorSpawned)) return;

	IndexToSpawn++;

	NewPlayer->Possess(ActorSpawned);
}

void ABlackmouthGamesGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void ABlackmouthGamesGameMode::TimerRespawnPlayer(AController* NewPlayer)
{
	if (!IsValid(GetWorld())) return;

	ControllersToRespawn.Add(NewPlayer);

	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(
		Timer,
		this,
		&ABlackmouthGamesGameMode::RespawnPlayer,
		TIME_TO_RESPAWN,
		false
	);
}

void ABlackmouthGamesGameMode::UpdateIndexToSpawn()
{
	IndexToSpawn = ++IndexToSpawn % 2;
}

void ABlackmouthGamesGameMode::PossessSpawnedActor(ACharacter* ActorSpawned)
{
	if (!IsValid(ActorSpawned) || !ControllersToRespawn.IsValidIndex(0)) return;

	ControllersToRespawn[0]->Possess(ActorSpawned);
	ControllersToRespawn.RemoveAt(0);
}

void ABlackmouthGamesGameMode::RespawnPlayer()
{
	if (!IsValid(DefaultPawnClass) || !IsValid(GetWorld())) return;

	ACharacter* ActorSpawned = GetWorld()->SpawnActor<ACharacter>(
		DefaultPawnClass,
		GetPlayerStart()
	);

	UpdateIndexToSpawn();
	PossessSpawnedActor(ActorSpawned);
}
