// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BLACKMOUTHGAMES_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(const float DeltaTime,
	                           const ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	int32 GetCurrentHealth() const { return Health; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(const int32 HealthToSet) { Health = HealthToSet; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	bool GetIsAlive() const { return Health > MinHealth; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealthPercentage() const;
	
	void SetupRespawnPlayer() const;
	void CharacterDeath() const;

	UFUNCTION(BlueprintCallable, Category = "Health")
	void TakeDamage(const int32 DamageToApply);

	void ShowDamage(const int32 DamageToShow) const;

	UFUNCTION(BlueprintCallable, Category = "Health")
    void CureHealth();

	void ShowCure(const int32 CureToShow) const;

	UFUNCTION(Server, Reliable)
	void HandleRespawnPlayerServer();
	void HandleRespawnPlayerServer_Implementation();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:

	UPROPERTY(Replicated)
	int32 Health = 0;
	
	int32 InitialHealth = 200;
	int32 MinHealth = 0;

	int32 HealthToCure = 10;
};
