// Fill out your copyright notice in the Description page of Project Settings.

#include "SphereAttackComponent.h"
#include "BlackmouthGamesCharacter.h"
#include "Components/CapsuleComponent.h"

USphereAttackComponent::USphereAttackComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SphereAttackCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereAttackCollision"));
}

void USphereAttackComponent::AssignBeginOverlap()
{
	if (!IsValid(SphereAttackCollision)) return;

	SphereAttackCollision->SetHiddenInGame(true);
	SphereAttackCollision->SetSphereRadius(InitialRadius);
	SphereAttackCollision->SetNotifyRigidBodyCollision(true);
	SphereAttackCollision->SetIsReplicated(true);

	SphereAttackCollision->OnComponentBeginOverlap.
	                       AddDynamic(this, &USphereAttackComponent::OnBeginOverlapSphereAttack);

	SphereAttackCollision->OnComponentEndOverlap.
	                       AddDynamic(this, &USphereAttackComponent::OnEndOverlapSphereAttack);
}

void USphereAttackComponent::SphereAttachComponent() const
{
	if (!IsValid(SphereAttackCollision)) return;

	SphereAttackCollision->AttachToComponent(
		GetCapsuleComponent(),
		FAttachmentTransformRules::KeepRelativeTransform
	);
}

void USphereAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	AssignBeginOverlap();
	SphereAttachComponent();
}

UCapsuleComponent* USphereAttackComponent::GetCapsuleComponent() const
{
	TArray<UCapsuleComponent*> SphereAttackMesh;
	GetOwner()->GetComponents(SphereAttackMesh);

	for (UCapsuleComponent* Component : SphereAttackMesh)
	{
		if (!IsValid(Component) || !Component->GetName().Contains("CollisionCylinder")) continue;

		return Component;
	}

	return nullptr;
}

float USphereAttackComponent::GetCurrentRadiusPercent()
{
	return
		(CurrentRadius - InitialRadius) / (TargetRadius - InitialRadius);
}

void USphereAttackComponent::RestoreCooldown()
{
	CurrentCooldownTime = InitialCooldownTime;
	bIsCooldown = false;
}

void USphereAttackComponent::UpdateCooldown()
{
	if (!bIsCooldown) return;

	if (CurrentCooldownTime <= 0)
	{
		bIsCooldown = false;
		RestoreCooldown();

		return;
	}

	CurrentCooldownTime--;

	TimerUpdateCooldown();
}

void USphereAttackComponent::TickComponent(const float DeltaTime,
                                           const ELevelTick TickType,
                                           FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsValid(SphereAttackCollision) ||
		!IsValid(GetWorld()) ||
		!bIsGrowingSphere ||
		SphereAttackCollision->GetUnscaledSphereRadius() == TargetRadius)
		return;

	const float RadiusToApply = FMath::FInterpConstantTo(
		SphereAttackCollision->GetUnscaledSphereRadius(),
		TargetRadius,
		GetWorld()->GetDeltaSeconds(),
		500.0f
	);

	CurrentRadius = RadiusToApply;

	if (GetOwner()->HasAuthority())
		HandleSphereScaleServer(RadiusToApply);
	else
		HandleSphereScaleClient(RadiusToApply);
}

void USphereAttackComponent::TimerUpdateCooldown()
{
	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(
		Timer,
		this,
		&USphereAttackComponent::UpdateCooldown,
		1.0f,
		false
	);
}

void USphereAttackComponent::SphereAttackBegin()
{
	if (bIsCooldown) return;

	bIsGrowingSphere = true;

	RestoreCooldown();
}

void USphereAttackComponent::RestoreSphereRadius()
{
	if (!IsValid(GetOwner())) return;

	if (GetOwner()->HasAuthority())
		HandleSphereScaleServer(InitialRadius);
	else
		HandleSphereScaleClient(InitialRadius);
}

void USphereAttackComponent::HandleDamageOverlappedActorsServer_Implementation(AActor* Enemy)
{
	if (!IsValid(Enemy)) return;

	ABlackmouthGamesCharacter* EnemyCharacter = Cast<ABlackmouthGamesCharacter>(Enemy);

	if (!IsValid(EnemyCharacter) || !IsValid(EnemyCharacter->GetHealthComponent())) return;

	EnemyCharacter->GetHealthComponent()->TakeDamage(SphereDamage);
}

void USphereAttackComponent::HandleDamageOverlappedActorsClient_Implementation(AActor* Enemy)
{
	if (!IsValid(Enemy)) return;

	HandleDamageOverlappedActorsServer(Enemy);
}

void USphereAttackComponent::DamageOverlappedActors()
{
	if (!IsValid(GetOwner())) return;

	for (AActor* Enemy : OverlappedActors)
	{
		if (GetOwner()->HasAuthority())
			HandleDamageOverlappedActorsServer(Enemy);
		else
			HandleDamageOverlappedActorsClient(Enemy);
	}

	OverlappedActors.Empty();
}

void USphereAttackComponent::SphereAttackEnd()
{
	if (bIsCooldown || !bIsGrowingSphere) return;

	bIsGrowingSphere = false;
	bIsCooldown = true;

	RestoreSphereRadius();
	DamageOverlappedActors();
	TimerUpdateCooldown();
}

void USphereAttackComponent::RestoreCurrentRadius(const float SphereRadius)
{
	if (SphereRadius == InitialRadius)
	{
		CurrentRadius = InitialRadius;
		SphereAttackCollision->SetHiddenInGame(true);
	}
}

void USphereAttackComponent::HandleSphereScaleServer_Implementation(const float SphereRadius)
{
	if (!IsValid(SphereAttackCollision)) return;

	SphereAttackCollision->SetSphereRadius(SphereRadius);
	SphereAttackCollision->SetHiddenInGame(false);

	RestoreCurrentRadius(SphereRadius);
}

void USphereAttackComponent::HandleSphereScaleClient_Implementation(const float SphereRadius)
{
	HandleSphereScaleServer(SphereRadius);
}

void USphereAttackComponent::AddOverlappedActor(AActor* OtherActor)
{
	if (!IsValid(OtherActor) || OverlappedActors.Find(OtherActor) != -1) return;

	OverlappedActors.Add(OtherActor);
}

void USphereAttackComponent::OnBeginOverlapSphereAttack(UPrimitiveComponent* OverlappedComponent,
                                                        AActor* OtherActor,
                                                        UPrimitiveComponent* OtherComp,
                                                        int32 OtherBodyIndex,
                                                        bool bFromSweep,
                                                        const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor) || !IsValid(GetOwner()) || OtherActor == GetOwner() || !bIsGrowingSphere) return;

	ABlackmouthGamesCharacter* CustomCharacter = Cast<ABlackmouthGamesCharacter>(GetOwner());

	if (!IsValid(CustomCharacter) || !CustomCharacter->IsLocallyControlled()) return;

	AddOverlappedActor(OtherActor);
}

void USphereAttackComponent::RemoveOverlappedActor(AActor* OtherActor)
{
	if (!IsValid(OtherActor) || OverlappedActors.Find(OtherActor) == -1) return;

	OverlappedActors.Remove(OtherActor);
}

void USphereAttackComponent::OnEndOverlapSphereAttack(UPrimitiveComponent* OverlappedComponent,
                                                      AActor* OtherActor,
                                                      UPrimitiveComponent* OtherComp,
                                                      int32 OtherBodyIndex)
{
	if (!IsValid(OtherActor) || !IsValid(GetOwner()) || OtherActor == GetOwner() || !bIsGrowingSphere) return;

	ABlackmouthGamesCharacter* CustomCharacter = Cast<ABlackmouthGamesCharacter>(GetOwner());

	if (!IsValid(CustomCharacter) || !CustomCharacter->IsLocallyControlled()) return;

	RemoveOverlappedActor(OtherActor);
}
