// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlackmouthGamesCharacter.h"
#include "Components/ActorComponent.h"
#include "Components/SphereComponent.h"
#include "SphereAttackComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BLACKMOUTHGAMES_API USphereAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USphereAttackComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(const float DeltaTime,
	                           const ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	void TimerUpdateCooldown();

	void SphereAttackBegin();
	void SphereAttackEnd();

	void RestoreCurrentRadius(float SphereRadius);
	void RestoreSphereRadius();

	void DamageOverlappedActors();

	UFUNCTION(NetMulticast, Reliable)
	void HandleDamageOverlappedActorsServer(AActor* Enemy);
	void HandleDamageOverlappedActorsServer_Implementation(AActor* Enemy);

	UFUNCTION(Server, Reliable)
	void HandleDamageOverlappedActorsClient(AActor* Enemy);
	void HandleDamageOverlappedActorsClient_Implementation(AActor* Enemy);

	UFUNCTION(BlueprintCallable, Category = "SphereAttack")
	float GetCurrentRadiusPercent();

	void RestoreCooldown();
	void UpdateCooldown();

	UFUNCTION(BlueprintCallable, Category = "SphereAttack")
	int32 GetCurrentCooldown() const { return CurrentCooldownTime; }

	UFUNCTION(BlueprintCallable, Category = "SphereAttack")
	bool GetIsCooldown() const { return bIsCooldown; }

	void AssignBeginOverlap();

	void SphereAttachComponent() const;

	UPROPERTY(EditDefaultsOnly, Category = "SphereAttack")
	USphereComponent* SphereAttackCollision = nullptr;

private:
	UFUNCTION(NetMulticast, Reliable)
	void HandleSphereScaleServer(const float SphereRadius);
	void HandleSphereScaleServer_Implementation(const float SphereRadius);

	UFUNCTION(Server, Reliable)
	void HandleSphereScaleClient(const float SphereRadius);
	void HandleSphereScaleClient_Implementation(const float SphereRadius);

	UFUNCTION()
	void OnBeginOverlapSphereAttack(UPrimitiveComponent* OverlappedComponent,
	                                AActor* OtherActor,
	                                UPrimitiveComponent* OtherComp,
	                                int32 OtherBodyIndex,
	                                bool bFromSweep,
	                                const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlapSphereAttack(UPrimitiveComponent* OverlappedComponent,
	                              AActor* OtherActor,
	                              UPrimitiveComponent* OtherComp,
	                              int32 OtherBodyIndex);

	void AddOverlappedActor(AActor* OtherActor);
	void RemoveOverlappedActor(AActor* OtherActor);

	UCapsuleComponent* GetCapsuleComponent() const;

	UPROPERTY(VisibleDefaultsOnly, Category = "SphereAttack")
	TArray<AActor*> OverlappedActors;

	float InitialRadius = 50.0f;
	float TargetRadius = 150.0f;
	float CurrentRadius = 0.0f;

	float InitialCooldownTime = 3.0f;
	float CurrentCooldownTime = 3.0f;

	bool bIsGrowingSphere = false;
	bool bIsCooldown = false;

	int32 SphereDamage = 15;
};
