// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Engine/TargetPoint.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/PlayerStart.h"
#include "Programs/UnrealLightmass/Private/ImportExport/3DVisualizer.h"


#include "BlackmouthGamesGameMode.generated.h"

UCLASS(minimalapi)
class ABlackmouthGamesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlackmouthGamesGameMode();

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<ACharacter> ActorToSpawn;

	void TimerRespawnPlayer(AController* NewPlayer);

protected:
	virtual void PostLogin(APlayerController* NewPlayer) override;

private:
	TArray<APlayerStart*> FindPlayerStart() const;
	FTransform GetPlayerStart() const;
	
	TArray<ATargetPoint*> FindTargetPointsToRespawn() const;

	void UpdateIndexToSpawn();
	void PossessSpawnedActor(ACharacter* ActorSpawned);

	void RespawnPlayer();
	void SpawnPlayer(APlayerController* NewPlayer);

	UPROPERTY(VisibleDefaultsOnly, Category = "Respawn")
	TArray<AController*> ControllersToRespawn;
	
	int32 IndexToSpawn = 0;
};
